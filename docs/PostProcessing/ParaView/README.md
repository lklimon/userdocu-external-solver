Postprocessing with ParaView
============================

[ParaView](https://www.paraview.org/) is a widely used visualisation software for simulation data.
It can be used to visualise field resutls from CFS.

Currently you require a [custom paraview plugin](../../Installation/ParaView.md#cfsreader-plugin) to load these result files.
