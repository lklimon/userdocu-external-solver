# MPP Muffler


Here we will perform an axisymmetric simulation concerning wave propagation in an acoustic muffler enhanced with a micro-perforated plate [MPP](https://en.wikipedia.org/wiki/Micro_perforated_plate) and 
perform a transmission loss computation as post-processing to evaluate the impact of the MPP on the effectivness of the muffler.

Thereby the wave is excited by acoustic normal velocity on line "excite" on the bottom $[x,y,z]=(0,0,0)$ of the domain.

For the analysis type, we choose a harmonic analysis for one frequency of 1000 Hz.

For the post-processing we need a non-reflecting boundary condition and in this example, we are using the PML (Perfectly Matched Layer) technique, described [here](../../../../Tutorials/Features/pml.md).

![acoustic](Rastergrafik.png)

---

## Meshing
The example also uses nonconforming interfaces [NMG](../../../../Tutorials/Features/ncinterfaces.md).

For this basic example the  mesh files are provided [here](muffler1NMG1.h5) and [here](muffler1NMG2.h5).

If you want to create you own geometry you can use the free Gmsh software [gmsh](https://gmsh.info/)

Then use your geometry and generate the meshes by executing
```shell
gmsh ur_geometry.geo -2 -v 0 -format msh2 -o ur_geometry.msh
```

---
## MPP material simulation
The simulation is based on the solving the [Helmholtz equation](../../../../PDEExplanations/Singlefield/AcousticPDE/) for the entire domain without the use of an impedance boundary condition.
In the air domain the value for bulk modulus (compression modulus) and air density is independent of frequency, but in the MPP domain a frequency dependent absorption 
behavior must be emulated. 
Therefore, we need a material model that provides the frequency depended values of the
acoustic density and bulk modulus for the MPP region. You can find a collection of appropriate models [here](https://apmr.matelys.com/). 

The provided material files are here [mppDensityR(eal)](mppDensityR.txt) [mppDensity(I)mag](mppDensityI.txt) [mppCompR(eal)](mppCompR.txt) [mppCompI(mag)](mppCompI.txt), with the values of the real and imaginary part of 
the equivalent density and bulk modulus of the MPP.

* Format for equivalent density and bulk modulus values *.txt files (Columns: frequency, real/imag part value)
```
50	12.9877351395964
55	14.2865086534968
60	15.5852821673803
.      .
.      .
```
* Defining MPP material in the xml-scheme. The frequency dependent values for the MPP-emulation are then linearly interpolated in the material [matMPP.xml](matMPP.xml) for each frequency step.
```
<densityComplex>
  <linear>
    <real> sample1D('mppDensityR.txt',f,1) </real>
    <imag> sample1D('mppDensityI.txt',f,1) </imag>
  </linear>
</densityComplex>
```
---

## XML Simulation Setup
The ready-to-use xml file is provided [here](Muffler1MPPnmg.xml).


Here, we will just discuss the main parts of the XML description file.

* Domains with regions (mesh scaled to mm) and nonconforming grid list and specifying evaluation points (set appropriate coordinate values) for computation of the transmission loss.
```
<fileFormats>
 <input>
   <hdf5 id="h1" scaleFac="1e-3" fileName="muffler1NMG1.h5"/>
   <hdf5 id="h2" scaleFac="1e-3" fileName="muffler1NMG2.h5"/>
 </input>
 <output>
  <hdf5/>
  <text id="txt"/>
 </output>
 <materialData format="xml" file="matMPP.xml"/>
</fileFormats>
<domain geometryType="axi">
  <regionList>
    <region name="airEl" material="air"/>
    <region name="damp" material="air"/>
    <region name="mpp" material="MPP1"/>
  </regionList>
  <nodeList>
    <nodes name="excitep">
      <coord x="0.0" y="1.2345" z="0.0"/>
    </nodes>
	<nodes name="surfinp">
      <coord x="0.0" y="1.2345" z="0.0"/>
    </nodes>
	<nodes name="surfOutp">
      <coord x="0.0" y="1.2345" z="0.0"/>
    </nodes>
  </nodeList>
  <ncInterfaceList>
   <ncInterface name="i1" slaveSide="I1slave" masterSide="I1master"/>
   <ncInterface name="i2" slaveSide="I2slave" masterSide="I2master"/>
  </ncInterfaceList>
</domain>
```

* Start the sequence and define analysis
```
<sequenceStep>
  <analysis>
    <harmonic>
      <numFreq> 1 </numFreq>
      <startFreq> 1000 </startFreq>
      <stopFreq> 1000 </stopFreq>
      <sampling>linear</sampling>
    </harmonic>
  </analysis>
```

* Define PDE for acoustics with nonconforming interface list
```
  <pdeList>
    <acoustic>
      <regionList>
        <region name="airEl"/>
        <region name="damp" dampingId="myPml"/>
		<region name="mpp" complexFluid="yes"/>
      </regionList>
	  <ncInterfaceList>
        <ncInterface name="i1" nitscheFactor="50" formulation="Nitsche"/>
		<ncInterface name="i2" nitscheFactor="50" formulation="Nitsche"/>
      </ncInterfaceList>  
```

* Define the PML (Perfectly Matched Layer) used for free field radiation
```
     <dampingList>
      <pml id="myPml">
        <type> inverseDist </type>
        <dampFactor>1.0</dampFactor>
      </pml>
    </dampingList>
```

* Define excitation and output result
```
    <bcsAndLoads>
      <normalVelocity name="excite" value="1"/>
    </bcsAndLoads>
   <storeResults>
      <nodeResult complexFormat="realImag" type="acouPressure">
        <allRegions/>
		<nodeList>
          <nodes name="excitep" outputIds="txt"/>
          <nodes name="surfinp" outputIds="txt"/>
          <nodes name="surfOutp" outputIds="txt"/>
        </nodeList>
      </nodeResult>
    </storeResults>
```




Perform the openCFS simulation by executing

```shell
cfs Muffler1MPPnmg
```


---

## Result
You can now investigate the results in paraview. For the computation of the transmission loss or reflection coefficient, use the *.txt output of the specified points as input for the
2-microphone method [DIN EN ISO 10534-2](https://www.din.de) or 3-microphone method [here](https://doi.org/10.1006/jsvi.1998.1800). 




