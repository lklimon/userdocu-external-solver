# Finite Element (FE) based Interpolation (beta)

*Warning: This filter is currently under development and not all functionality is fully tested.*

The Finite Element (FE) based Interpolation filter computes the **R**ight **H**and **S**ide force $f$ (computation of the source $f$ is described in [@schoder2020c]) of an partial differential equation

\begin{equation}
f \varphi \, ,
\end{equation}

using first order nodal FEM to solve the system of equations, similar to the [conservative Interpolators](../ConservativeInterpolators/README.md) [@schoder2020a].

![Conservative Interpolation Cell Centroid](cfs_caa.JPG)


The filter is defined as following in the CFSdat xml input:

```

<interpolation type="FieldInterpolation_FEBased" id="interpolation" inputFilterIds="input" >
	<fetype conservative="..." hundredPercent="..." connectivity="..." useFV="..." maxDistance="..." numNeighbours="..." exponent="..."/>
	<useElemAsTarget>...</useElemAsTarget>
 
	<targetMesh>
		<hdf5 fileName="targetMesh.h5"/>
	</targetMesh>
	<singleResult>
		<inputQuantity resultName="..."/>
		<outputQuantity resultName="..."/>
	</singleResult>
	<regions>
		<sourceRegions>
			<region name="..." />
		</sourceRegions>
		<targetRegions>
			<region name="..."/>
		</targetRegions>
	 </regions>
</interpolation>

```

The input values are taken either from the nodes or from the elements (centroids).

In **fetype**, the following optional attributes may be adjusted:

 * **conservative**: for conservative interpolation the elements in the target grid are found in which the source points (node coordinates of element centroids) lay in, then the shape function of the element nodes are evaluated at the input point to get the interpolation coefficients. for non-conservative interpolation the search is done in reversed order. The target points are located in the source elements and the shape functions are evaluated from these elements.
	* In case of **hundredPercent** interpolation source elements/nodes are searched for, if no source element was found for a target. This can be done either by connectivity or nearest neighbours (defined by **connectivity**)
 
 * **hundredPercent**:  If true, also target elements/nodes are searched for, if the sources are not located inside a target element
 
	* if **connectivity** is true, the neighbours of sources which are evaluated successfully are evaluated and the non-interpolated sources get targets accordingly. The neighbour search is done either based on finite elements (element->node->element - **useFV**=False) or finite volumes (element->face->element - **useFV**=true)
	* if the distance from source to target exceeds **maxDistance** only **numNeighbours** neighbours are taken into account to avoid too big matrices. At such places there should be no relavant data.
	* if **connectivity** is false, **numNeigbhours** defines how many nearest neighbours are searched for to make the interpolation conservative
   
 * **exponent**: The option exponent regulates the weightings in case of **hundredPercent** option is needed to find a corresponding source or target. The higher "exponent" is , the higher is the weighting for the near replacements in contrast to the far away ones (see also *interpolationExponent* in [Nearest Neigbour Interpolator](../NN/README.md))
   
 In **useElemAsTarget**, a bool is defined optionally, whether the target entity are Nodes (false=default) or Elements (true)

---

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [FE-based Interpolation]._

---

# References
\bibliography