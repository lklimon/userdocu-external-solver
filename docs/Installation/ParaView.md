Installing ParaView and activating the CFSReader Plugin
=======================================================

In order to read openCFS native HDF5 data format (`*.cfs` files) with paraview you need our **CFSReader plugin**.
The plugin has been recently included into paraview and needs to be activated before it can be used.

ParaView
--------

You can build ParaView from source or install an [official nightly version of the ParaView release](https://www.paraview.org/download/) for your operation system.

**Note:** The **nightly version** can be found on the **Version** drop down box

CFSReader Plugin
----------------

In order to read openCFS result files (`*.cfs` files) with paraview, one must activate **CFSReader plugin**.

Follow these steps:

  * Open ParaView and in the menu bar click **Tools > Manage Plugins**

  * Select **CFSReader** and click the **Load Selected** button (you can also activate the **Auto Load** check box to load the plugin on every paraview start)

  * Now **.cfs** files can be opened and visualized using ParaView










