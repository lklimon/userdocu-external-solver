# External Eigensolver

An external script can be used to solve standard eigenvalue problems $\boldsymbol{A}\boldsymbol{x}=\lambda\boldsymbol{x}$ and generalized eigenvalue problems $\boldsymbol{A}\boldsymbol{x}=\lambda\boldsymbol{B}\boldsymbol{x}$. 
This external eigensolver can be called via the eigenvalue or eigenfrequency analysis step. The [1D Oscillator with External Eigensolver](../../Applications/Singlefield/Mechanics/1DOscillator/README.md) application illustrates the usage of this feature.

## Overview
![a1](FlowchartOverview.jpg)

The call of the external eigensolver and the transferred information depends on the following sections in the XML-scheme: 

In the `<analysis>` section, one can choose:

* between eigenvalues (`<eigenValue>` step) or eigenfrequencies (`<eigenFrequency>` step), where the relations $\omega = 2 \pi f$ and $\omega = \sqrt{\lambda}$ hold for the standard and generalized eigenvalue problem.

* the type of eigenvalue problem (standard, generalized) and assign the system matrices (only for the `<eigenvalue>` step). 

* a region for which a certain number of eigenvalues should be computed, e.g. via the options `<inInterval>` or `<valuesAround>` for the `<eigenvalue>` step.

Based on this information, the ExternalEigenSolver class handles the system matrix export and sets up the terminal call to the external script.
The solver settings for the external eigensolver are defined in the `<linearSystems>` section. 
This information is directly processed in the ExternalEigenSolver classand passed to the external script as terminal arguments. 
Please refer to the section on the XML scheme for a list of possible solver settings. 
The system matrices are exported to ASCII files in the [matrix-market format](#matrix-market-format) (`.mtx`) format in the working directory. 
The external script loads the system matrices from these files, solves the eigenvalue problem and saves the results in `.mtx` files. 
From those files, the eigenvalues and eigenvectors can be imported back into openCFS ([see Import function](#import-function)).   

## XML-Scheme
In the `<linearSystems>` section, the external eigensolver is defined by the `<external>` tag and a couple of required and optional arguments can be defined.
The general settings are:

* `<logging>`: determines if the output of the external script is shown in the console. 
Default is no.
* `<cmd>`: defines the command to call the external script via the terminal. 
This argument is always required!
* `<deleteFiles>`: specifies if the system matrices and the result files (in `.mtx` format) should be deleted after all computations are completed.
Default is yes.

The arguments in the `<arguments>` section are appended as positional arguments to the script call in the order specified by the user. 
The following entries are supported:

* `<min>`/`<max>`: defines the intervall in which the eigenvalues should lie. 
The actual values are specified in the `<analysis>` section. 
* `<shiftPoint>`: specifies the shift point that should be used to find eigenvalues. 
The actual value is defined in the `<analysis>` section. 
Note that it is possible to define a format for the output of complex numbers at this point with the `formatString` attribute, e.g. `shiftPoint formatString="%.6f%+.6fj"`. 
The format is a string with placeholders, as used by the [printf()](https://en.cppreference.com/w/cpp/io/c/fprintf) function in C++. 
The default format is "%.6f%+.6fj", corresponding to the python format for complex numbers, with 6 digits after the decimal point. 
* `<number>`: indicates how many eigenvalues should be computed. 
The actual value is defined in the `<analysis>` section.
* `<tolerance>`: gives the user the possibility to specify a tolerance. 
This tolerance is required by openCFS for the evaluation of error bounds, but python eigensolvers do not return a tolerance. 
However, they often allow for the definition of a precision up to which they converge. 
Default is machine precision.
* `<AFileName>`: defines the name of the `.mtx` file storing the $\boldsymbol{A}$ matrix of the above defined eigenvalue problems. 
If only the tag is given, the default name "jobname_AMatrix" is used (the job name is defined through the openCFS call).
* `<BFileName>`: defines the name of the `.mtx` file storing the $\boldsymbol{B}$ matrix of the generalized eigenvalue problem. 
If only the tag is given, the default name "jobname_BMatrix" is used.
* `<EigenValuesFileName>`: defines the name of the `.mtx` file storing the eigenvalues. 
If only the tag is given, the default name "jobname_EigenValues" is used.
* `<EigenVectorsFileName>`: defines the name of the `.mtx` file storing the eigenvectors. 
If only the tag is given, the default name "jobname_EigenVectors" is used.

The following definiton example of the external eigensolver 
```xml
<external id="ext">
  <logging>yes</logging>
  <cmd>python3 EigenSolver.py</cmd>
  <arguments>
    <AFileName/>
    <shiftPoint/>
    <number/>
    <tolerance>1e-9</tolerance>
  </arguments>
</external>
```
would lead to a terminal call of the kind:

`python3 EigenSolver.py job_AMatrix.mtx -2.500000+1.500000j 4 1.000000e-09`

## Matrix Market Format
The Matrix Market format [@MatrixMarketDesign] (`.mtx` file extension) is used for the exchange of system matrices and results between openCFS and the external eigensolver. 
When writing the script fo the external eigensolver, it might be useful to have some notions of how this format is composed, so this section tries to give a short introduction.
  
All `.mtx` files start with a header holding the format type (array or coordinate format), the entry type (real, complex) and the matrix type (general, symmetric, hermitian).
A typical example would be:
```
%%MatrixMarket matrix coordinate real general
```
This header is followed by a line indicating the matrix dimensions and the number of non-zero entries (only for coordinate format). 
Afterwards, the matrix entries are listed, depending on the storage type and entry type.

The real valued matrix $\boldsymbol{A} = (\begin{matrix}1.5 & 0\\ 0 & 7.89\end{matrix})$ is stored in coordinate format as
```
2 2 2
1 1 1.5
2 2 7.89
```
Similarly the complex valued matrix $\boldsymbol{B} = (\begin{matrix}-0.5+2j & 9.2\\ 3.4 & 4.5+0.5j\end{matrix})$ is stored in array format as:
```
2 2 
-0.5 2
3.4  0
9.2  0
4.5  0.5
```

Note that the array format only makes sense for dense matrices, while the coordinate format is mostly used for sparse matrices. 


## External Eigensolver
Python offers some very powerful libraries for linear algebra problems. 
Thus the documentation will only cover python scripts, even though other types of external scripts are also possible.
The `scipy.io` module even contains functions for importing (`mmread()`) `.mtx` files to Python arrays or exporting (`mmwrite()`) the result arrays to `.mtx` files. 
The following functions can be useful for solving eigenvalue problems:

* `numpy.linalg.eig`: get eigenvalues and eigenvectors for the standard eigenvalue problem, relies on the LAPACK routines [@LapackUserGuide] 
* `scipy.linalg.eig`: get eigenvalues and eigenvectors for the standard or generalized eigenvalue problem, relies on the LAPACK routines
* `scipy.sparse.linalg.eigs`: get eigenvalues and eigenvectors for the standard or generalized eigenvalue problem using sparse matrices, relies on the ARPACK routines [@ArpackUserGuide]

For more information regarding input arguments and alternative function calls please refer to the respective API references for [NumPy](https://numpy.org/doc/stable/reference/index.html) and [SciPy](https://docs.scipy.org/doc/scipy/reference/index.html).
A template script can be found [here](../../Applications/Singlefield/Mechanics/1DOscillator/README.md).

## Import Function
The Import function imports the eigenvalues and eigenvectors in Matrix Market format to a vector in openCFS.
Without covering the implementation in detail, the following flowchart aims at providing some understanding of the import process, which might be necessary for debugging the result export in the external script.

![a2](FlowchartImport.jpg) 

The Import function checks if the entry type corresponds to the expected numerical type (real or complex) and stores the result values in a data structure that can be further processed in CFS.

## References







