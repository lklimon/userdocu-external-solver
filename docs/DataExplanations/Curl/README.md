# Curl

The curl operator calculates the curl $\mathbf{r}$ of a source vector $\mathbf{s}$.

$\mathbf{r} = \nabla \times \mathbf{s}$

Therefore the derivative is performed on radial basis functions (see [Radial Basis Funtions](../RBF/README.md)). 
The [theoretical background](https://doi.org/10.1002/nme.6298) of the implemented RBF interpolation scheme is published in [@schoder2020c]. When using the conservative filters please provide a **citation** in you publication:

_Schoder, Stefan, et al. "Aeroacoustic source term computation based on radial basis functions."  International Journal for Numerical Methods in Engineering (2020)._

The filter is defined as following in the CFSdat xml input:

```
<differentiation type="SpaceDifferentiation_Curl" inputFilterIds="input" id="curl">
  <RBF_Settings epsilonScaling="..." betaScaling="..." kScaling="..." logEps="..."/>
  <targetMesh>
	<hdf5 fileName="targetMesh.h5"/>
  </targetMesh>
  <singleResult>
	<inputQuantity resultName="..."/>
	<outputQuantity resultName="..."/>
  </singleResult>
  <regions>
	<sourceRegions>
	  <region name="..."/>
	</sourceRegions>
	<targetRegions>
	  <region name="..."/>
	</targetRegions>
  </regions>
</differentiation>
```

In **RBF_Settings**, the following (optional and mandatory) attributes may be adjusted:

* **epsilonScaling** (mandatory): Controls the "smoothness" of the basis function. The smoother the Gauss-like surface is, the better the results will be BUT only until a certain number, when the matrix becomes so ill-conditioned, which will either result in an exception or very bad results. Typical values ~0.1

* **betaScaling**: Slope of the linear term that was added to the radial basic functions

* **kScaling**: constant term that was added to the radial basic functions

* **logEps**: Console output of [minimal distance, maximal distance, optimized epsilon]. NOTE: Should only be used for investigating the quality of the derivative, because it produces a LOT of console output.

---

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [Curl]._

---

# References
\bibliography