# Heat sink


Here we perfom the computation of the temperature field and the occuring mechanical deformation of a CPU heat sink. 

<img src="heatSinkSetup.png" width="550">

Thermal boundary conditions are given as follows:

* $\Gamma_s$: set heat flux to $216666.67\,$W/mK
* $\Gamma_i$: set to thermal isolation
* $\Gamma_r$: radiation with $h=70$W/m and $T_{\rm r}=20^\circ$C

and for the computation of the mechanical deformation we apply full support on $\Gamma_s$, meaning to set the displacement $\mathbf{u}$ to zero on this surface.

The geometry is already provided by a [STL-file](heatsink.STEP), which can be directly imported in [trelis](https://coreform.com/products/cubit-trelis/) via the command `import`. All files for this example can be downloaded [here](heatsink.zip).


## Meshing
To obtain a highly qualitative mesh, we use the *SubMap* algorithm in **trelis**. 

<img src="heatSinkmesh.png" width="350">

With the commands ```block``` and ```nodeset``` we define our computational volume and the surfaces at which we describe the boundary conditions. The complete journal file [heatsink.jou](heatsink.jou) includes the reading of the STL-file, the meshing and exporting as an ANSYS-cdb file. After the cdb-file is written to disc, you can convert it to the standard ```cfs```-file with the [convert_mesh.xml](convert_mesh.xml) and executing
```
cfs -p convert_mesh.xml -g heatsink
```

---

## XML Simulation Setup
The ready-to-use xml file is provided [here](heatMechSink.xml) as well as the [material xml file](mat.xml).


Here, we will just discuss the main parts of the XML description file.

* Input: we use a scaling of $10^{-3}$ to get the coordinates in meter.
```
  <input>
      <hdf5 fileName="heatsink.cfs" scaleFac="1e-3"/>
  </input>
```

* Domain: we just need to perform the simulation for the aluminium block
```
  <domain geometryType="3d">
      <regionList>
          <region name="V_alu" material="alu"/>
      </regionList>
  </domain>
```

* The first sequece is a static analysis to obtain the temperture field
```
  <sequenceStep index="1">
      <analysis>
          <static/>
      </analysis>
      <pdeList>
          <heatConduction>
              <regionList>
                  <region name="V_alu" polyId="lagrange"/>
              </regionList>
              <bcsAndLoads>
                  <heatFlux name="S_source" value="216666.67"/>                    
                  <heatTransport name="S_radiation" volumeRegion="V_alu" bulkTemperature="20" heatTransferCoefficient="75"/>
              </bcsAndLoads>
              .....
            </heatConduction>
        </pdeList>
    </sequenceStep>
```

* The second sequence step performs the computation of the mechanical deformation
```
    <sequenceStep index="2">
    <analysis>
        <static/>
    </analysis>
    
        <pdeList>
            <mechanic subType="3d">
               <regionList>
                   <region name="V_alu" polyId="lagrange"/>
               </regionList>
               <bcsAndLoads>
                  <thermalStrain name="V_alu">
                      <sequenceStep index="1">
                          <quantity name="heatTemperature" pdeName="heatConduction"/>
                          <timeFreqMapping>
                              <constant/>
                          </timeFreqMapping>
                      </sequenceStep>
                  </thermalStrain>
                  <fix name="S_source">
                      <comp dof="x"/>
                      <comp dof="y"/>
                      <comp dof="z"/>
                   </fix>
               </bcsAndLoads>
                
               <storeResults>
                ....
             </mechanic>
        </pdeList>    
    </sequenceStep>
```

Perform the openCFS simulation by executing

```shell
cfs heatMechSink
```


---

## Result
You can now investigate the results in paraview

* **Temperature distribution**

<img src="heatSinkTemp.png" width="450">

* **Mechanial displacement on deformed structure**

<img src="heatSinkDisp.png" width="450">





