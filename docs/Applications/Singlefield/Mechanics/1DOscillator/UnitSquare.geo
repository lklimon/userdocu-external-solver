// This script generates a square plate with a structured mesh consisting of a single element.

// define constants
l = 1.0; // characteristic length
d = 0.5; // default mesh size at points

// set element type (1st order hexahedral elements)
Mesh.ElementOrder = 1;
Mesh.RecombineAll = 1;

// define corner points
Point(1) = {-0.5*l, -0.5*l, 0, d};
Point(2) = { 0.5*l, -0.5*l, 0, d};
Point(3) = { 0.5*l,  0.5*l, 0, d};
Point(4) = {-0.5*l,  0.5*l, 0, d};

// connect points by lines 
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// mesh lines with 1 element
Transfinite Line{1,3} = 1;
Transfinite Line{2,4} = 1;

// create a closed surface
Line Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};

// mesh surface
Transfinite Surface{1};

// define physical groups for nodes, edges and surfaces that are relevant for the simulation
Physical Point('N_concentrated-element') = {1}; // corner point for BC 
Physical Line('S_fixed') = {4}; // corner edges for BC
Physical Surface('V_square') = {1}; // surface for computational domain

// export mesh 
Save "UnitSquare.msh";

