# General Information

Establishing an xml file for CFSdat it is fundamental that the pipeline, existing of different CFSdat filters, is closed. The pipeline has to start with the step value definition and has to be followed by the input filter and end with the output filter. In between multiple filters can be added, serially or parallelly.

## Defining Step Value Definition
It is possible to define input data for the time and frequency domain. However, not all filters are capable of processing data in the frequency domain. 
```
  <pipeline>
  
     <stepValueDefinition>
      <startStop>
        <startStep value="..."/>
        <numSteps value="..."/> 
        <startTime value="..."/>
        <delta  value="..."/>
        <deleteOffset  value="no"/>
      </startStop>
    </stepValueDefinition>

  </pipeline>    
```    

## Defining Filters
 
| Single or serial Filters: | Multiple, parallel Filters: |
| ----------- | ----------- |
| ![pipeline](pipeline.png) | ![MultiPipeline](MultiPipeline.png) |



</br>
** Which can be defined in the xml as: ** </br>
Single or serial Filters:
```
  <pipeline>
  
     <stepValueDefinition>
      <startStop>
        <startStep value="..."/>
        <numSteps value="..."/> 
        <startTime value="..."/>
        <delta  value="..."/>
        <deleteOffset  value="no"/>
      </startStop>
    </stepValueDefinition>
    
    <meshInput id="inputFilter" gridType="fullGrid"  >      

    </meshInput>
    
    <interpolation type="FieldInterpolation_Cell2Node" id="interp1" inputFilterIds="inputFilter">
    </interpolation>       
    
    <meshOutput id="Outout" inputFilterIds="interp1">

    </meshOutput>
  </pipeline>
```
</br>
Multiply, parallel Filters:
```
  <pipeline>
  
     <stepValueDefinition>
      <startStop>
        <startStep value="..."/>
        <numSteps value="..."/> 
        <startTime value="..."/>
        <delta  value="..."/>
        <deleteOffset  value="no"/>
      </startStop>
    </stepValueDefinition>
    
    <meshInput id="inputFilter" gridType="fullGrid"  >      

    </meshInput>

    <interpolation type="FieldInterpolation_Cell2Node" id="interp1" inputFilterIds="inputFilter">
    </interpolation> 
       
    
    <interpolation type="FieldInterpolation_Cell2Node" id="interp2" inputFilterIds="inputFilter">
    </interpolation>    
    
    
    <interpolation type="FieldInterpolation_Cell2Node" id="interp3" inputFilterIds="inputFilter">
    </interpolation>       
    
    <meshOutput id="Outout" inputFilterIds="interp1 interp2 interp3">

    </meshOutput>
  </pipeline>
```





** Link all CFSdat Filter to Testsuite examples **

Give overview with short explanation of all possible Filter



## Available Filters

### Interpolation Filters

Each interpolation filter, that is not conservatively, can be performed in the time and frequency domain.  

* ** [Cell2Node/Node2Cell](../N2CC2N/README.md) **</br>
Allows to interpolate nodal based data onto cells and vice versa. 


* ** [Nearest Neighbour](../NN/README.md) **
* ** [FE Based](../FEBased/README.md) **
* ** [Radial Basis Functions](../RBF/README.md) **

### Conservative Interpolation Filters
* ** [CellCentroid](../ConservativeInterpolators/README.md) **  </br>
Allows conservatively interpolating scalar quantities.
* ** [CutCell](../ConservativeInterpolators/README.md) ** </br>
Allows conservaively interpolating scalar quantities, using an advanced cut cell algorithm. Requires more computational ressources than CellCentroid.  

### Aeroacoustic Source Terms
* ** [LambVector](../LambVector/README.md) ** </br>
AeroacousticSource_LambVector
* ** [LighthillSourceTerm](../LighthillSourceTerm/README.md) ** </br> 
AeroacousticSource_LighthillSourceTerm
* ** [LighthillSourceVector](../LighthillSourceTermVector/README.md) ** </br> 
AeroacousticSource_LighthillSourceVector
* ** [TimeDerivative](../TimeDerivative/README.md) ** </br>
deriv

### Synthetic Sources
* ** [SNGR](../SNGR/README.md)** </br>

### Data Processing
* **[Curl](../Curl/README.md)** </br>
SpaceDifferentiation_Curl
* ** [Divergence](../Divergence/README.md)** </br> 
SpaceDifferentiation_Divergence
* ** [Gradient](../Gradient/README.md)** </br>
SpaceDifferentiation_Gradient
* ** [TimeMean](../TimeMean/README.md)** <br>
timeMean
* ** [FIR](../FIR/README.md)**
* ** [Temporal Blending](../TempBlend/README.md)**
* ** [VolumeMultiplication](../VolMulti/README.md)**

 

# References
\bibliography
