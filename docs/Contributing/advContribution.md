To get started editing the documentation (locally on your machine) the following steps are required

1. clone/fork the [userdocu source repository](https://gitlab.com/openCFS/userdocu).
2. install [MkDocs](https://www.mkdocs.org/) (The mkdocs style is based on markdown language and super easy to edit).
3. edit in the source, view the result locally.
4. commit your changes to your forked repository, and push it to the gitlab server.
5. create a merge request on gitlab, assign it to a project maintainer for review.

In the following the steps are explained in more detail.

## Prerequisits

To start contributing you need the following:

* [**Gitlab-account:**](#creating-a-gitlab-account)
You need to have a gitlab-account to get access to the [Userdocumentation-repository](https://gitlab.com/openCFS/userdocu)
* [**Git:**](#installing-git)
A version control program. It runs in the background of git and you can use it for obtaining the repostory, changing it and merge back to the original project (and many more).
* [**Anaconda/python:**](#installing-anaconda)
MkDocs needs pyhton and anaconda is open-source-distribution of python, which contains many helpful programs, for e.g. Jupyter-Notebook which can be used to create and edit markdown-files.
* [**MkDocs:**](#installing-mkdocs)
The enviroument on which the Userdocumention-Website is build. With mkDocs you can host your local server and see instantly the changes you make to the userdocumentation. This is a great way to see if everything works as intendet.

### Creating a Gitlab-account
Simply create a Gitlab-account [here](https://gitlab.com/users/sign_up).
#### Create and add ssh-key to gitlab account
Add a ssh-key to your Gitlab-accaunt to enable automatic authentification (no longer annonying password requests).

* **Under Ubuntu:**
Follow this  [guide](https://gitlab.com/-/profile/keys) on gitlab.com
* **Under Windows:**
Follow these [instruction](http://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key). As terminal use the git bash.
If standard settings are used, your public ssh-key should be in `C:\Users\YourUsername\.ssh`.
The public key looks something like this: `key_name.pub` Open the public key with the texteditor, copy the key and add it to your [account](https://gitlab.com/-/profile/keys)

### Installing Git

[What is git?](https://www.coredna.com/blogs/what-is-git-and-github-part-two)

Here are some slides of the basic of git: [Git-Info-Slides](slides_git.pdf)

* **Under Ubuntu:**
Install git with the command: `# sudo apt install git`
* **Under Windows:**
Download git from http://git-scm/download/win and install it with standard settings. This installs the **git-bash**. In the following commands which have to be executed in the git bash be marked with a `G#` in front of the commando (eg.: as `G# git status`).

### Installing Anaconda
* **Under Ubuntu:**
https://docs.anaconda.com/anaconda/install/linux/
* **Under Windows:**
https://docs.anaconda.com/anaconda/install/windows/
Now you have also an **anaconda bash**. In the following commands which have to be executed in the git bash be marked with a `A#` in front of the commando (eg.: as `A# pip install mkdocs`).

### Installing MkDocs
You can install MkDocs (and all requirements) with the following code:
```
# pip install -r requirements.txt
```
Here is the [requirements.txt](https://gitlab.com/openCFS/userdocu/-/blob/master/requirements.txt).
For more details look [here](https://note.nkmk.me/en/python-pip-install-requirements/).
Keep in mind to use the correct terminal if you use windows.

Alternativly you can install each package on its own:

* **Under Ubuntu:**
Install following packages:
    - Mkdocs: `# pip install mkdocs `
    - LaTex rendering: `# pip install pymdown-extensions`
    - Bibtex bibliographs: `# pip install mkdocs-bibtex`
    - mkdocs-material: `# pip install mkdocs-material `
* **Under Windows:**
Open the Anaconda promt and execute following commandos:
    - Mkdocs: `A# pip install mkdocs`
    - LaTex rendering: `A# pip install pymdown-extensions`
    - Bibtex bibliographs: `A# pip install mkdocs-bibtex`
    - mkdocs-material: `A# pip install mkdocs-material`

## Obtain the repository
Please check if you have all [prerequisits](#prerequisits).

If you dont have directly access to the project (which is most likley) you have to fork the projekt.
This means you simply mirroring the project into your namespace, and therefore gaining all rights on your namespace.
### How to fork
Go the the [userdocumentation repository](https://gitlab.com/openCFS/userdocu) and click the [`fork`-button](https://gitlab.com/openCFS/userdocu/-/forks/new) and follow the instructions. **Make sure that your fork is public.**

On the forked repository you have now all rights and you are able to edit everything. **Do not worry about deleting/destroying anything**, if something stopped working you can simply reset everything, either by:

* deleting your forked repository and do all steps (forking & cloning) again. 
* or by using git: The commando `# git restore .` restores everything since your last commit.

### Clone the repository

Go to the (forked) userdocumentation repository on [gitlab.com](https://gitlab.com). It should be listed under _Projects => Your projects_.  Click onto the `Clone`-button and copy the `<ssh-link>`/`<https-link>`. The links should look like this:

* `<ssh-link>`: "git@gitlab.com:YOURNAMESPACE/userdocu.git" 
* `<https-link>`: "https://gitlab.com/YOURNAMESPACE/userdocu.git"

* **Under Ubuntu:** 
    - Open the terminal and go to the directory where the repository should be (use `# cd path/to/folder`)
    - clone the repostiory with this command: `# git clone <ssh-link or https-link>`
    
* **Under Windows:** 
    - Open the git bash and go to the directory where the repository should be (use `G# cd path/to/folder`)
    - clone the repository with this command: `G# git clone  <ssh-link or https-link>`
    
Now the repository should be locally on your computer.

With **git** you can now manage your repository. Some here are some useful commands:

* `git add .`: Adds new files and changes for your next commit.
* `git add filename`: Add a specific file to your next commit.
* `git commit`: Commiting all changes (which are added with `git add`) to your repository.
* `git checkout -b NewBranche`: Creates a new Branche called `NewBranche`
* `git checkout BrancheName`: Switches to the Branche called `BrancheName`. By default the master-branche is called `master`.
* `git pull`: Updates your local repostiory with the origin repository (the one on gitlab).
* `git push`: Pushes your local repository onto the original repository (the one on gitlab).

For a more detailed explaination please have a look into these [slides](slides_git.pdf) and dont hesitate to use google ;) .

### Git with Graphical User Interface

It is possible to use Git via GUI by installing the Eclipse Git Team provider, thus being a simple entry for the unexperienced, since Git-commands, datastructure and CFS-code become integrated in one user interface.
This gives easy access to the openCFS-Testsuite, with many practical applications of the PDEs explained, as well.

An installation tutorial and user documentation is available [here](https://www.vogella.com/tutorials/EclipseGit/article.html).




## Editing the Userdocumentation

### Starting the virtual mkDocs-Server

* **Under Ubuntu**: Open the terminal and go to the path where your local repository is stored and execute `# mkdocs serve`
* **Under Windows**: Open the anaconda promt and go to the path where your local repository is stored and execute `A# mkdocs serve`

This generates a virtual Server, which can be visited with a browser (just like an ipython-notebook).
Just copy the http address [http://127.0.0.1:8000](http://127.0.0.1:8000) in your browser and the documentation is displayed.

Leave the browser open and start editing the markdown-files in the /docs folder.
By saving these files, the browser-page gets automatically updated...what you see is what you get principle (no compiling or building needed)
### Basic Structure
The basic structure of the docu is defined in the .yml-file `mkdocs.yml` which is located in the top level folder. Everything else is located in the `/docs` folder, otherwise mkdocs cannot build it.

#### Adding a Section/Subsection
If you need to create a new category, simply add these in the `mkdocs.yml` in the navigation section.
```yml
#This is a comment
#Naviation section. Insert here new sections/subsections.
nav:
    - openCFS: 'index.md' 
    - History: 'history.md'
    #Freshly created Section
    - My Section:
    	#Undersection with the path to the markdown-file
    	#Note that the folder 'MySection' needs to be created in '/docs/'						
        - My new Undersection: 'MySection/myMarkdownfile.md'	
    - Installation: 
        - Overview: 'Installation/README.md'
        - Pre-Processors: 'Installation/PrePorcessors.md'
        - XML Editors: 'Installation/XML-Editors.md'
        - openCFS: 'Installation/CFS.md'
        - ParaView Plugin: 'Installation/ParaView.md'

```
If needed create a new subfolder in `/docs` and copy your `Markdownfile.md` in this directory.
These changes are displayed live on the hosted mkdocs server as well.


#### Using Jupyter Notebook
One can use jupyter notebooks directly to create markdownfiles. This is especially helpful to see if mathematical expressions are displayed correctly. You can use the same syntax you would use for a latex file. For example:
```latex
\begin{equation}
\int_a^b \frac{1}{x} dx
\end{equation}
```
will be displayed as
\begin{equation}
\int_a^b \frac{1}{x} dx
\end{equation}
 Code snipplets can be displayed as well. And if you click the button on the right side of the snipplet, you simply copy it to you clipboard.
```python
list1 = [i for i in range(10) if i < 5]
list2 = [i**i for i in list]
```
To export the jupyernotebook to a markdown-file simply go to:

 * _File-> Export Notebook as... -> Export Notebook as Markdown_

Now you have created a markdownfile, which only needs to be put into the right place.
Alternativly you can use any texteditor to edit a markdown-file.


## Create a merge Request
In order to merge your repository into the original repository, your branch/forked repository have to be merged into the original repository.

### Im finished with editing. What should i do?
Great. Now you have to do following steps:

* [Commit and push everything to your repository](#updating-your-local-repository)
* [Create a merge request](#create-merge-request)
* [Mark it as ready](#mark the mergerequest as ready)

#### Updating your local repository
Im just running quickly over some basics. For more details look into [git-slides](slides_git.pdf).

* With `# git status` you can see the current status of your repostitory (branchname, filechanges etc.). Here you can see if your local repostiory is up to date.
* With `# git add file` you can add a change/file to your repository
* With `# git commit` you commit these adds to the repostiory. Note: Now you have to write a short commit message e.g. "Added XYZ".
* With `# git push` you push your local repository to gitlab.

### Create Merge Request

Once you pushed your branch onto gitlab and it passes the pipeline, its time to create your merge request.
#### Follow the steps in the gif below:
* Click `merge request` on the sidebar
* Click `new merge request`
* Select source branche: This is your forked repo e.g `your_fork/userdocu : master`
* Select target branche: This is `openCFS/userdocu : master` (the branch your fork is based on) 
* Click `Compare cranches and continue`
* Add a meaningful title and description
* Create merge request
* Hint: assign yourself as assignee, so you find the merge request on gitlab under `Merge requests`.

![merge-gif](merge.gif)

### Mark the mergerequest as ready
Once:

* the merge request is created
* you are done with all the work 
* merge request passes the pipeline (automatic tests to garantuee you do not blow something up)

you can mark the merge request as ready. Click on `Mark as ready`.

### Assign a reviewer

Once your merge-request is marked as ready, you should assign a developer as reviewer (@apapst, @kroppert, etc..), so we get notified a new merge-request is ready to review.
If the merge request gets approved, your branche will get merged into the master and deployed on the userdocumentation website.
![AssignReviewer](reviewer.jpg)

If you are not able to assign a developer, contact them via email.
